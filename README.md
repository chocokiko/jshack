#A roguelike game for your browser in JavaScript#


##Featuring:##
* Libraries
    * [Jquery](http://jquery.com)
    * [Jquery UI](http://jquery-ui.com)
    * [GameQuery](http://gamequeryjs.com)
    * [Qunit Testing](http://qunitjs.com/)
    * [MooTools Class inheritance](http://mootools.net/)
* Techniques
    * Loading JS, JSON, CSS, HTML Libraries via AJAX with custom loader
      and loadingbar
    * Preloading Images, Animations and Sounds via AJAX with loadingbar via
      GameQuery resource manager


##Credits:##
* [Nethack - One of the greatest games I ever played and the inspiration](http://www.nethack.org)
* [The Nethack Wiki - for endles spoilers](http://www.nethackwiki.com)
* [How to Write a Roguelike in 15 Steps](http://www.roguebasin.com/index.php?title=How_to_Write_a_Roguelike_in_15_Steps)
* ["CSS3 Progress Bars" by Chris Coyier on css-tricks.com](https://css-tricks.com/css3-progress-bars/)
* [Stack Overflow for all sorts of help with stupid questions](http://stackoverflow.com)
* [Bullet Points - About the only thing that keeps me organized in some fashion](https://en.wikipedia.org/wiki/Bullet_%28typography%29#Bullet_points)
* [Bitbucket](http://bitbucket.org)
* [Markdown - for easy Documentation](http://stationinthemetro.com/wp-content/uploads/2013/04/Markdown_Cheat_Sheet_v1-1.pdf)


##Todos:##
* Tidy up loadingDemo.html as far as possible
* merge loadingDemo.html with main game


##Files:##
###.:###

| File                | Comment                                                    |
| ------------------- | ---------------------------------------------------------- |
| css                 | folder with own stylesheets                                |
| examples            | folder with different unchanged 3rd party examples         |
| images              | folder with images for the general layout go here          |
| js                  | folder with all the js I wrote for the game                |
| json                | folder with JSON data filesfor the game                    |
| tilesets            | folder with different free Nethack tilesets to provide     |
|                     | themeableness                                              |
| lib                 | folder with unchanged 3rd party libraries used in the game |
| demo.html           | main game file for now                                     |
| loadingDemo.html    | demonstrator to develop AJAX library loading               |
|                     | - will get removed after integration into main game        |
| README.md           | this file                                                  |


###./css:###

| File                    | Comment                      |
| ----------------------- | ---------------------------- |
| loader.css              | Initial Loadingscreen Styles |


###./examples:###

| File                | Comment                                                                                                                                      |
| ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| antfarm             | folder with [Simulating an Ant Farm in JavaScript](https://thiscouldbebetter.wordpress.com/2013/01/26/simulating-an-ant-farm-in-javascript/) |
| nethack-3.4.3       | folder with [Nethack Sourcecode](http://www.nethack.org)                                                                                     |
| ProgressBars        | folder with ["CSS3 Progress Bars" by Chris Coyier on css-tricks.com](https://css-tricks.com/css3-progress-bars/)                             |

###./images:###

| File                | Comment                                                         |
| ------------------- | --------------------------------------------------------------- |
| compass.svg         | Wikimedia Commons Image to be used for character movement input |


###./js:###

| File                    | Comment                                                      |
| ----------------------- | ------------------------------------------------------------ |
| dependencies.data.js    | Array of dependencies to be loaded durin first loading phase |
| gamequery.extend.js     | Gamequery extension to load tilemaps from JSON data          |
| loader.javascript.js    | Custom AJAX dependency loader                                |
| massive.javascript.js   | 2 MB of JS to test loading - to be removed                   |


###./json:###

| File                    | Comment           |
| ----------------------- | ----------------- |
|                         |                   |


### ./lib:###

| File                                        | Comment                         |
| ------------------------------------------- | ------------------------------- |
| jquery.gamequery-0.7.1.js                   | Gamequery library               |
| jquery-ui-1.11.4.custom                     | folder with Jquery UI Library   |
| MooTools-Core-1.5.2-compat-compressed.js    | MooTools Library compressed     |
| MooTools-Core-1.5.2.js                      | MooTools Library human readable |
| qunit-1.20.0.css                            | QUnit Library Stylesheet        |
| qunit-1.20.0.js                             | Qunit Library JS                |


###./tilesets:###

| File                             | Comment                                             |
| -------------------------------- | --------------------------------------------------- |
| absurd128.png                    | Nethack Absurd Tileset 128px                        |
| absurd32.png                     | Nethack Absurd Tileset 32px                         |
| absurd64.png                     | Nethack Absurd Tileset 64px                         |
| absurd72.png                     | Nethack Absurd Tileset 72px                         |
| DawnHack 16px.png                | DawnHack Tileset 16px                               |
| DawnHack 24px.png                | DawnHack Tileset 24px                               |
| DawnHack 32px.png                | DawnHack Tileset 32px                               |
| gltile16.png                     | SDL Nethack Tileset 16px                            |
| gltile32_transparent.png         | SDL Nethack Tileset 32px transparent background     |
| gltile64_transparent_3d.png      | SDL Nethack Tileset 64px transp bground pseudo 3d   |
| nevanda_nethack32.png            | Nevanda Nethack Tileset 32px                        |
| nh32.bmp                         | ??? Tileset - needs checking                        |
| nh32.xpm                         | ??? Tileset - needs checking                        |
| nh343-nextstep10.png             | NextStep Tileset 10px                               |
| tiles32.css                      | CSS to make tiles accessible JQuery UI style        |
| tiles32.png                      | Nethack X11 Tileset                                 |
| tilesabsurd96.png                | Nethack Absurd Tileset 96px                         |
| untekhack32.png                  | Untekhack Tileset 32px                              |
| x11bigtiles32.png                | Nethack X11 Tileset 32px                            |
| x11tiles16.png                   | Nethack X11 Tileset 16px                            |

