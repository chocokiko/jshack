/**
 * Setup game elements
 * @return {[type]} [description]
 * @todo needs heavy refactoring !!
 */
function setupPlayground(){
    // Anchor for GameQuery playing field
    // Append Playground to DOM
    $('#main').append(
        '<div id="playground"></div>'
    );

    // Game and Debug messages go here
    $('#main').append(
        '<div id="messages"></div>'
    );

    // Mocha Unittesting Area
    $('#main').append(
        '<div id="mocha"></div>'
    );

    // Setup basic layers
    $('#playground').playground({
        // height: conf.playground.height,
        // width: conf.playground.width,
        overflow: 'hidden'
    })
    .css('position', 'initial')
    .addGroup("map", {
        // width: conf.playground.width,
        // height: conf.playground.height,
        overflow: 'hidden'
    }).z(50).end()
    .addGroup("object", {
        // width: conf.playground.width,
        // height: conf.playground.height,
        overflow: 'hidden'
    }).z(60).end()
    .addGroup("entity", {
        // width: conf.playground.width,
        // height: conf.playground.height,
        overflow: 'hidden'
    }).z(70);
}
