/**
 * Class to handle 'constants'
 * To be used as singleton
 *
 *
 */
var Const = new Class({
    // simulate private members by name obfuscation
    // similar to Python
    private_dont_access_fuck_off: {
        // put constants here
        data: {},
    },
    /**
     * Constructor
     * @return void
     */
    initialize: function(){
        // no need to initialize for a singleton
    },
    /**
     * Define a 'constant'
     * @param  {String} key   constant name
     * @param  {mixed}  value constnt value
     * @return {Boolean}      -> true
     * @throws ConstantException
     */
    define: function(key, value){
        // constant already exists
        if (this.isDef()) {
            throw {
                name: 'ConstantException',
                message : 'Constant already exists'
            }
        }

        // More sanity checks go here

        with (this.private_dont_access_fuck_off) {
            // lastly set constant
            data['key']= value;
        }
        return true;
    },
    /**
     * checks if constant is defined
     * @param  {string}  key constant name
     * @return {Boolean}     true if constant exists
     */
    isDef: function(key){
        with (this.private_dont_access_fuck_off) {
            return data.keys().indexOf(key) != -1;
        }
    },
    /**
     * retrieve constant value
     * @param  {String} key constant name
     * @return {mixed}      constant value
     * @throws ConstantException
     */
    get: function(key){
        if (!this.isDef()) {
            throw {
                name: 'ConstantException',
                message : 'Constant does not exist'
            }
        }

        with (this.private_dont_access_fuck_off) {
            return data.keys().indexOf(key) != -1;
        }
    }
});
