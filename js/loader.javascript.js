/**
 *  Callback function after libs are loaded
 *
 */
var wrapUpLoading = function () {
    // update texts
    // $('.loading_area h2').html('Loading game resources...').fadeIn(400);
    $('.loading_area .message strong').html('Preparing Game...');

    // setup the game itself, now that all tools are at hand
    setupGame();

    // fade out loading bar overlay for good
    // @todo not sure if this will work here with all that async bullshit going on
    // $('.loading_area').fadeOut(400, function() {
    //     $( this ).remove();
    // });

    // @todo removeme
    // alert('finished_loading');
}


/**
 *  Load libraries via AJAX from list and run callback when done
 *
 *  @param libraries    List of libraries with params to load
 *  @param doneCallback Callback Function when done loading
 */
var loadLibraries = function(libraries, doneCallback){

    /**
     *  update loading bar smoothly with sliding animation
     *
     *  @param bar          selector for loading bar to update
     *  @param origWidth    original bar width
     *  @param newWidth     new bar width
     *
     *  @todo origWith might not be needed; try to take it out
     */
    var animate_loadingbar = function(bar, origWidth, newWidth){
        $(bar)
            .width(origWidth)
            .animate(
                {
                    width: newWidth
                },
                200
            );
    }

    // Initialize variables for percentage calculations
    totalsize = 0;
    currentsize = 0;

    // calculate total libs size to use as 100%
    for (var i = 0; i < libraries.length; i++) {
        totalsize += libraries[i].size
    };

    // Damn Javascript for just working with async loop
    var counter = 0;
    (function asyncLoop() {
        // load via AJAX depending on library type
        switch(libraries[counter].type) {
            case 'js':
                $.ajax({
                    url: libraries[counter].url,
                    // js has to be loaded as 'script' and is run automatically
                    // after loading
                    dataType: "script",
                    async: false,
                    cache: false
                });
                break;
            case 'css':
                $.ajax({
                    url:libraries[counter].url,
                    // css is treated as plain text;
                    dataType:"text",
                    async: false,
                    cache: false,
                    success:function(data){
                        // CSS is only parsed after adding it to the DOM
                        $("head").append("<style>" + data + "</style>");
                    }
                });
                break;
            case 'json':
                $.ajax({
                    url:libraries[counter].url,
                    // JSON is parsed and chacked automatically but not run
                    dataType:"json",
                    async: false,
                    cache: false,
                    success:function(data){
                        // register loaded data in the JSON "namespace"
                        window.JSON[libraries[counter].name] = data;
                    }
                });
                break;
            case 'html':
                $.ajax({
                    url:libraries[counter].url,
                    // JSON is parsed and chacked automatically but not run
                    dataType:"html",
                    async: false,
                    cache: false,
                    success:function(data){
                        // append it to the DOM at hidden area to be processed
                        // further
                        $('#hidden_area').append(
                            $(data)
                        );
                    }
                });
                break;
            default:
                // default code block
                // @todo treat unknown types as plain text and assign to variable
        }

        // update currently loaded size to calculate percentage of total size
        currentsize += libraries[counter].size;

        // Display what we are actually loading for user amusement
        $('.loading_area .message strong').text(libraries[counter].title);

        // update loadingbar smoothly to current percentage
        animate_loadingbar(
            '#loading_bar span',
            $('#loading_bar span').width(),
            Math.round(currentsize / totalsize * 50)+'%'
        );

        // update async loop counter
        counter++;
        if (counter < libraries.length) {
            // run loop again
            setTimeout(asyncLoop, 300);
        } else {
            // bail out and fire done callback
            setTimeout(doneCallback, 500);
        }
    })();
}



