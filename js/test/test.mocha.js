/**
 * Run Mocha Tests
 * @return void
 */
function mochaTests(){
    mocha.setup({
        ui:'bdd'
    })

    describe('Unittests', function(){
        describe('Mocha', function(){
            it('should work without error', function() {
                (true).should.equal(true);
            });
        });

        describe('Pending', function(){
            it('should have more tests');
        })

        describe('MooTools', function() {
            /**
             * Test constant class
             */
            it('should create a working Class', function(){
                var Const = new Class({
                    // simulate private members by name obfuscation
                    // similar to Python
                    private_dont_access_fuck_off: {
                        // put constants here
                        data: {},
                    },
                    /**
                     * Consructor
                     * @return void
                     */
                    initialize: function(){
                        // constructor
                    },
                    /**
                     * Define a 'constant'
                     * @param  {String} key   constant name
                     * @param  {mixed}  value constnt value
                     * @return {Boolean}      -> true
                     * @throws ConstantException
                     */
                    define: function(key, value){
                        // constant already exists
                        if (this.isDef()) {
                            throw {
                                name: 'ConstantException',
                                message : 'Constant already exists'
                            }
                        }

                        // More sanity checks go here

                        with (this.private_dont_access_fuck_off) {
                            // lastly set constant
                            data['key']= value;
                        }
                        return true;
                    },
                    /**
                     * checks if constant is defined
                     * @param  {string}  key constant name
                     * @return {Boolean}     true if constant exists
                     */
                    isDef: function(key){
                        with (this.private_dont_access_fuck_off) {
                            return data.keys().indexOf(key) != -1;
                        }
                    },
                    /**
                     * retrieve constant value
                     * @param  {String} key constant name
                     * @return {mixed}      constant value
                     * @throws ConstantException
                     */
                    get: function(key){
                        if (!this.isDef()) {
                            throw {
                                name: 'ConstantException',
                                message : 'Constant does not exist'
                            }
                        }

                        with (this.private_dont_access_fuck_off) {
                            return data.keys().indexOf(key) != -1;
                        }
                    }
                });

                (new Const).should.be.an.instanceOf(Object);
                (true).should.equal(true);
            });
        });
    });


    mocha.checkLeaks();
    mocha.globals(['jQuery']);
    mocha.run();

};

