$.fn.extend({
    /**
     * This function imports JSON  generated by Tiled (http://www.mapeditor.org/).
     * All the created tilemaps will be directly under the currently selected element.
     * Their name will be made of the provided prefix followed by a number starting at 0.
     *
     * Only layer of type "tilelayer" are supported for now. Only one single tileset
     * per layer is supported.
     *
     * After the call to this function the second argument will hold two new arrays:
     * - tiles: an arrays of tilemaps wraped in jQuery.
     * - animations: an arrays of animations
     *
     * This is a non-destructive call
     */
    importTilemapJSON: function(json, prefix, generatedElements){
        var animations = [];
        var tilemaps = [];

        var that = this;

        var tilemapJsonLoaded = function(json){
            var tilesetGID = [];
            for (var i = 0; i < json.tilesets.length; i++) {
                tilesetGID[i] = json.tilesets[i].firstgid;
            }

            var getTilesetIndex = function(index){
                var i = 0;
                while(index >= tilesetGID[i] && i < tilesetGID.length){
                    i++;
                }
                return i-1;
            }

            var height = json.height;
            var width  = json.width;
            var tileHeight = json.tileheight;
            var tileWidth  = json.tilewidth;

            var layers = json.layers;
            var usedTiles = [];
            var animationCounter = 0;
            var tilemapArrays = [];

            // Detect which animations we need to generate
            // and convert the tiles array indexes to the new ones
            for (var i=0; i < layers.length; i++){
                if(layers[i].type === "tilelayer"){
                    var tilemapArray = new Array(height);
                    for (var j=0; j<height; j++){
                        tilemapArray[j] = new Array(width);
                    }
                    for (var j=0; j < layers[i].data.length; j++){
                        var tile = layers[i].data[j];
                        if(tile === 0){
                            tilemapArray[Math.floor(j / width)][j % width] = 0;
                        } else {
                            if(!usedTiles[tile]){
                                animationCounter++;
                                usedTiles[tile] = animationCounter;
                                animations.push(new $.gameQuery.Animation({
                                    imageURL: json.tilesets[getTilesetIndex(tile)].image,
                                    offsetx: ((tile-1) % Math.floor(json.tilesets[getTilesetIndex(tile)].imagewidth / tileWidth)) * tileWidth,
                                    offsety: Math.floor((tile-1) / Math.floor(json.tilesets[getTilesetIndex(tile)].imagewidth / tileWidth)) * tileHeight
                                }));
                            }
                            tilemapArray[Math.floor(j / width)][j % width] = usedTiles[tile];
                        }
                    }
                    tilemapArrays.push(tilemapArray);
                }
            }
            // adding the tilemaps
            for (var i=0; i<tilemapArrays.length; i++){
                 tilemaps.push(that.addTilemap(
                    prefix+i,
                    tilemapArrays[i],
                    animations,
                    {
                        sizex:  width,
                        sizey:  height,
                        width:  tileWidth,
                        height: tileHeight
                }));
            }
        };

        tilemapJsonLoaded(json);

        if(generatedElements !== undefined){
            generatedElements.animations = animations;
            generatedElements.tilemaps = tilemaps;
        }

        return this;
    }
});