// provide a place to store loaded JSON in a "namespaced" fashion
if (typeof(window.JSON) == 'undefined') {
    window.JSON={};
}

// add the javascript and CSS dependencies to the JSON
window.JSON.libraries= [
    // JQuery related libs
    {
        'title': 'Jquery UI',
        'url':   'http://chocokiko.bitbucket.org/jquery/jquery-ui-1.11.4.custom/jquery-ui.js',
        'type':  'js',
        'size':  470596
    },
    {
        'title': 'Jquery UI Css',
        'url':   'http://chocokiko.bitbucket.org/jquery/jquery-ui-1.11.4.custom/jquery-ui.css',
        'type':  'css',
        'size':  36839
    },
    {
        'title': 'GameQuery',
        'url':   'http://chocokiko.bitbucket.org/jquery/gamequery/jquery.gamequery-0.7.1.js',
        'type':  'js',
        'size':  85761
    },
    {
        'title': 'GameQuery Extension',
        'url':   'http://chocokiko.bitbucket.org/js/gamequery.extend.js',
        'type':  'js',
        'size':  3883
    },

    // Unittesting with Mocha
    {
        'title': 'MochaJs',
        'url':   'https://cdn.rawgit.com/mochajs/mocha/2.2.5/mocha.js',
        'type':  'js',
        'size':  112454
    },
    {
        'title': 'MochaJs CSS',
        'url':   'https://cdn.rawgit.com/mochajs/mocha/2.2.5/mocha.css',
        'type':  'css',
        'size':  5349
    },
    {
        'title': 'ShouldJS',
        'url':   'https://cdn.rawgit.com/shouldjs/should.js/7.1.1/should.js',
        'type':  'js',
        'size':  112454
    },

    // Unittests themselves
    {
        'title': 'Unittests',
        'url':   'js/test/test.mocha.js',
        'type':  'js',
        'size':  112454
    },

    // MooTools and Classes
    {
        'title': 'MooTools Core / Class',
        'url':   'http://chocokiko.bitbucket.org/jquery/moo-tools/MooTools-Core-1.5.2.js',
        'type':  'js',
        'size':  23727
    },
    // {
    //     'title': 'Massive JS for testing',
    //     'url':   'http://chocokiko.bitbucket.org/js/massive.javascript.js',
    //     'type':  'js',
    //     'size':  2112076
    // },

    // Game elements
    {
        'title': 'Tile CSS tiles32',
        'url':   'tilesets/tiles32.css',
        'type':  'css',
        'size':  80037
    },
    {
        'title': 'Controls HTML',
        'url':   'html/controls.part.html',
        'type':  'html',
        'size':  2631
    },
    {
        'title': 'Example Tilemap',
        'name':  'tilemap',
        'url':   'json/example.tilemap.json',
        'type':  'json',
        'size':  3249
    },
    {
        'title': 'Playground Setup',
        'url':   'js/setup.playground.js',
        'type':  'js',
        'size':  20000
    }

];

